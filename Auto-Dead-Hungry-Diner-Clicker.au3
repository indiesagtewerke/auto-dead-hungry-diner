#cs ----------------------------------------------------------------------------

 AutoIt Version: 3.3.14.5
 Author:         Quintin Henn

 Script Function:
	Automate the game Dead Hungry Diner.

#ce ----------------------------------------------------------------------------

#include <AutoItConstants.au3>
#include <MsgBoxConstants.au3>
#include <Misc.au3>

Const $appName = "Auto Dead Hungry Diner Clicker"

Const $loops = 33

Global $escapeKeyCode = "1B"
Global $hDLL = DllOpen("user32.dll")

MsgBox($MB_ICONINFORMATION, $appName, "Starting. Waiting for Dead Hungry Diner to become active...")

Local $hWnd = WinWaitActive("[TITLE:Dead Hungry Diner; CLASS:OgreGLWindow;]")

Func CheckEscPressed()
  If _IsPressed($escapeKeyCode, $hDLL) Then
    return True
  EndIf
  Return False
EndFunc   ;==>CheckEscPressed

Func ClickFirstCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 585, 1) ;~ first seat
EndFunc   ;==>ClickFirstCustomer

Func ClickThirdCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 585, 1) ;~ third seat
EndFunc   ;==>ClickFirstCustomer

Func ClickNextLevel()
  MouseClick($MOUSE_CLICK_PRIMARY, 1035, 740, 1) ;~ next level
EndFunc   ;==>ClickNextLevel

Func ClickGoHome()
  MouseClick($MOUSE_CLICK_PRIMARY, 885, 740, 1) ;~ level select screen
  MouseClick($MOUSE_CLICK_PRIMARY, 300, 200, 1) ;~ go to home screen
EndFunc   ;==>ClickGoHome

Func ClickFirstColumnCustomers()
  ClickFirstCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 440, 1) ;~ second seat
EndFunc   ;==>ClickFirstColumnCustomers

Func ClickSecondColumnCustomers()
  ClickThirdCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 440, 1) ; fourth seat
EndFunc   ;==>ClickSecondColumnCustomers

Func ClickTables()
  ClickFirstColumnCustomers()
  ClickSecondColumnCustomers()
EndFunc   ;==>ClickTables

Func PlaceOrder()
  MouseClick($MOUSE_CLICK_PRIMARY, 1320, 380, 1) ;~ prepair food
EndFunc   ;==>PlaceOrder

Func CollectOrders()
  ClickFirstColumnCustomers()
  PlaceOrder()

  ClickSecondColumnCustomers()
  PlaceOrder()
EndFunc   ;==>CollectOrders

Func CollectFood()
  MouseClick($MOUSE_CLICK_PRIMARY, 1320, 520, 1) ;~ collect food
EndFunc   ;==>CollectFood

Func ServeCustomers()
  CollectFood()
  ClickFirstColumnCustomers()

  CollectFood()
  ClickSecondColumnCustomers()
EndFunc   ;==>ServeCustomers

Func GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 780, 1) ;~ left customer
EndFunc   ;==>GetLeftCustomer

Func RunABusiness()
  Sleep(5000)

  CollectOrders()
  Sleep(3000)
  ServeCustomers()

  Sleep(7000)
  ClickTables() ;~ collect gold
  Sleep(2000)
  ClickTables() ;~ clean tables
EndFunc   ;==>RunABusiness

Func SkipDailog()
  MouseClick($MOUSE_CLICK_PRIMARY, 950, 890, 1) ;~ skip button
EndFunc   ;==>SkipDailog

Func SkipStory($skipCount)
  MouseClick($MOUSE_CLICK_PRIMARY, 1400, 750, $skipCount) ;~ next
EndFunc   ;==>SkipStory

Func StartLevel()
  MouseClick($MOUSE_CLICK_PRIMARY, 1020, 700, 1) ;~ start level
EndFunc   ;==>StartLevel

Func IsEscPressed()
  return _IsPressed($escapeKeyCode, $hDLL)
EndFunc   ;==>IsEscPressed

;~ MouseMove(1050, 600) ;~ story mode
;~ MouseMove(560, 380) ;~ first row
;~ MouseMove(560, 475) ;~ second row
;~ MouseMove(1590, 200) ;~ play button
;~ MouseMove(1010, 700) ;~ start level

;~ MouseMove(1320, 380) ;~ prepair food
;~ MouseMove(1320, 520) ;~ collect food

;~ MouseMove(1050, 780) ;~ right customer
;~ MouseMove(880, 780) ;~ left customer

;~ MouseMove(880, 585) ;~ first seat
;~ MouseMove(880, 440) ;~ second seat
;~ MouseMove(1120, 585) ;~ third seat
;~ MouseMove(1120, 440) ;~ fourth seat

#cs ----------------------------------------------------------------------------

 Create a new profile

#ce ----------------------------------------------------------------------------
Func CreateNewProfile()
  MouseClick($MOUSE_CLICK_PRIMARY, 400, 200, 1) ;~ open profile screen
  MouseClick($MOUSE_CLICK_PRIMARY, 950, 200, 1) ;~ select second profile
  MouseClick($MOUSE_CLICK_PRIMARY, 1400, 200, 1) ;~ delete profile
  MouseClick($MOUSE_CLICK_PRIMARY, 860, 720, 1) ;~ confirm delete
  MouseClick($MOUSE_CLICK_PRIMARY, 340, 200, 1) ;~ home screen
  MouseClick($MOUSE_CLICK_PRIMARY, 980, 580, 1) ;~ story mode
  Sleep(900)
  MouseClick($MOUSE_CLICK_PRIMARY, 1020, 700, 1) ;~ casual mode
  MouseClick($MOUSE_CLICK_PRIMARY, 1580, 200, 1) ;~ play
EndFunc   ;==>CreateNewProfile

#cs ----------------------------------------------------------------------------

 Level 1

#ce ----------------------------------------------------------------------------
Func FirstLevel()
  SkipStory(16)
  Sleep(1000)
  StartLevel()

  Sleep(1000)
  SkipDailog()

  GetLeftCustomer()
  ClickFirstCustomer() ;~ seat zombie

  Sleep(1000)
  SkipDailog()

  Sleep(5000)
  SkipDailog()

  ClickFirstCustomer()
  PlaceOrder()

  Sleep(4000)
  SkipDailog()

  CollectFood()
  ClickFirstCustomer()

  Sleep(1500)
  SkipDailog()

  Sleep(6000) ;~ collect gold
  SkipDailog()
  ClickFirstCustomer()

  Sleep(2000) ;~ clean table
  SkipDailog()
  ClickFirstCustomer()

EndFunc   ;==>FirstLevel

#cs ----------------------------------------------------------------------------

 Level 2

#ce ----------------------------------------------------------------------------
Func SecondLevel()
  Sleep(1000)
  SkipStory(1)
  StartLevel()

  Sleep(1200)
  SkipDailog()

  GetLeftCustomer() ;~ seat zombies
  ClickFirstCustomer()

  GetLeftCustomer()
  ClickThirdCustomer()

  SkipDailog()
  Sleep(9000)
  ClickFirstCustomer()
  ClickThirdCustomer()
  PlaceOrder()

  Sleep(4500)
  CollectFood()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(12000) ;~ collect gold
  SkipDailog()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(1000) ;~ clean table
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000) ;~ seat zombies
  GetLeftCustomer()
  ClickThirdCustomer()
  GetLeftCustomer()
  ClickFirstCustomer()

  Sleep(9500)
  ClickFirstCustomer()
  ClickThirdCustomer()
  PlaceOrder()

  Sleep(5000)
  CollectFood()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(12000) ;~ collect gold
  SkipDailog()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000) ;~ clean table
  ClickFirstCustomer()
  ClickThirdCustomer()

  #cs ----------------------------------------------------------------------------

  first red zombie

  #ce ----------------------------------------------------------------------------

  Sleep(3000) ;~ seat zombies
  GetLeftCustomer()
  ClickFirstCustomer()

  GetLeftCustomer()
  ClickThirdCustomer()

  Sleep(9000)
  ClickFirstCustomer()
  ClickThirdCustomer()
  PlaceOrder()

  Sleep(5000)
  CollectFood()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(12000) ;~ collect gold
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000)
  SkipDailog()

  Sleep(3000) ;~ clean table
  ClickFirstCustomer()
  ClickThirdCustomer()

  #cs ----------------------------------------------------------------------------

  red zombies

  #ce ----------------------------------------------------------------------------

  Sleep(3000) ;~ seat zombies
  GetLeftCustomer()
  ClickThirdCustomer()
  GetLeftCustomer()
  ClickFirstCustomer()

  Sleep(8000)
  ClickFirstCustomer()
  ClickThirdCustomer()
  PlaceOrder()

  Sleep(5000)
  CollectFood()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(10000) ;~ collect gold
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000) ;~ clean table
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000) ;~ seat zombies
  GetLeftCustomer()
  ClickThirdCustomer()
  GetLeftCustomer()
  ClickFirstCustomer()

  Sleep(8000)
  ClickFirstCustomer()
  ClickThirdCustomer()
  PlaceOrder()

  Sleep(5000)
  CollectFood()
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(10000) ;~ collect gold
  ClickFirstCustomer()
  ClickThirdCustomer()

  Sleep(3000) ;~ clean table
  ClickFirstCustomer()
  ClickThirdCustomer()

EndFunc   ;==>SecondLevel

#cs ----------------------------------------------------------------------------

 Level 3

#ce ----------------------------------------------------------------------------
Func ThirdLevel()
  Sleep(1000)
  StartLevel()
  Sleep(1000)
  SkipDailog()

  RunABusiness()

  Sleep(2000)

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 440, 1) ;~ second seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 585, 1) ;~ first seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 440, 1) ;~ fourth seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 585, 1) ;~ third seat

  RunABusiness()

  Sleep(2000)

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 440, 1) ;~ second seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 585, 1) ;~ third seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 880, 585, 1) ;~ first seat

  GetLeftCustomer()
  MouseClick($MOUSE_CLICK_PRIMARY, 1120, 440, 1) ;~ fourth seat

  RunABusiness()
  Sleep(8000)
EndFunc   ;==>ThirdLevel


#cs ----------------------------------------------------------------------------

 Dead Hungry Diner

#ce ----------------------------------------------------------------------------
Local $running = true

Sleep(8000)
;~ TODO: Allow ESC to exit the application
For $loopCount = 1 To $loops Step 1
  CreateNewProfile()
  FirstLevel()
  Sleep(5000)
  ClickNextLevel()
  SecondLevel()
  Sleep(5000)
  ClickNextLevel()
  ThirdLevel()
  Sleep(2000)
  ClickGoHome()
  Sleep(1500)
Next

MsgBox($MB_SYSTEMMODAL, $appName, "You have looped " & ($loopCount - 1) & " times!")
Local $response = MsgBox($MB_YESNO, $appName, "Dead Hungry Diner' now?")
If ($IDYES == $response) Then
  WinClose($hWnd)
EndIf
