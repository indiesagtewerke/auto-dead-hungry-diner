# Auto DeadHungry Diner Clicker

Auto click through the first 3 levels of the game 'Dead Hungry Diner'. This auto clicker script creates a new profile each time it starts, this allows the app to earn stars for the same 3 levels.

## Requirements

- Windows operating system
- [AutoIt v3](https://www.autoitscript.com/site/) installed

## How to use

1. Run this clicker application script `"C:\Program Files (x86)\AutoIt3\AutoIt3.exe" Auto-Dead-Hungry-Diner-Clicker.au3`.
2. Close the dailog that confirms that the script started.
3. Open 'Dead Hungry Diner' from Steam, the clicker script will start clicking through the game.
4. Press the 'Esc' key to stop the script's execution earlier than the specified amount of clicks in the script.

## Tools used for Development

- Click automation developed with the [AutoIt3][autoit] scripting language.

[autoit]: https://www.autoitscript.com/site/
